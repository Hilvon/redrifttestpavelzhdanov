using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpriteDisplay : MonoBehaviour
{
[SerializeField]
    private Image _Display;
    [SerializeField]
    private CardDataHolder _DataHolder;
    
    
    private void Awake() {
        _Display.sprite = _DataHolder.Portrait;
        _DataHolder.Portrait.OnValueUpdated += OnValueUpdated;
    }

    private void OnValueUpdated(Sprite newValue) {
        _Display.sprite = newValue;
    }
}
