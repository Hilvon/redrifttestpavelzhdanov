using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class CardDropZone : MonoBehaviour, IDropHandler
{
    void IDropHandler.OnDrop(PointerEventData eventData) {
        var card = eventData.pointerDrag.GetComponent<CardDragManager>();
        card?.ConfirmDropzone(this);
        
    }

}
