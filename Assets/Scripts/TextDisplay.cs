using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDisplay : MonoBehaviour
{
    public enum DisplayedProperty { Title, Description }

    [SerializeField]
    private Text _Display;
    [SerializeField]
    private CardDataHolder _DataHolder;
    [SerializeField]
    private DisplayedProperty _TargetProperty;

    protected ReactiveVarible<string> PickValue(CardDataHolder dataHolder) {
        switch (_TargetProperty) {
            case DisplayedProperty.Title: return dataHolder.Title;
            case DisplayedProperty.Description: return dataHolder.Description;
            default: return null;
        }
    }
    private void Awake() {
        var targetProperty = PickValue(_DataHolder);
        _Display.text = targetProperty.Value;
        targetProperty.OnValueUpdated += OnValueUpdated;
    }

    private void OnValueUpdated(string newValue) {
        _Display.text = newValue;
    }
}
