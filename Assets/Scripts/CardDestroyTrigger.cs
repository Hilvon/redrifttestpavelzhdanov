using UnityEngine;

public class CardDestroyTrigger : MonoBehaviour
{
    [SerializeField]
    private CardDataHolder _DataHolder;
    
    void Start()
    {
        _DataHolder.HP.OnValueUpdated += CheckDestroyedCondition;
        CheckDestroyedCondition(_DataHolder.HP);
    }

    private void CheckDestroyedCondition(int hp) {
        if (hp <= 0) {
            Destroy(gameObject);
        }
    }
}
