using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasCameraSetup : MonoBehaviour
{
    private static Camera _CachedMainCamera;
    private static Camera _MainCamera {
        get {
            if (_CachedMainCamera == null)
                _CachedMainCamera = Camera.main;
            return _CachedMainCamera;
        }
    }
    [SerializeField]
    private Canvas _Canvas;
    void Start()
    {
        _Canvas.worldCamera = _MainCamera;
    }
}
