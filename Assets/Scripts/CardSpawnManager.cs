using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSpawnManager : MonoBehaviour
{
    [SerializeField]
    private CardClassManager _ClassManager;

    public CardDataHolder CreateCard(string className) {
        var cardData = _ClassManager.GetClass(className);
        var cardInstance = Instantiate(CardTemplateManager.GetTemplate(cardData.TemplateType), transform);
        cardInstance.SetupCardData(cardData);
        return cardInstance;
    }
}
