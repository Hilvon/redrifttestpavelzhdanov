using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSmoothMovementHandler : MonoBehaviour
{
    [SerializeField]
    private CardDataHolder _TargetPosition;

    private void Start() {
        transform.SetParent(_TargetPosition.transform.parent);
    }

    public bool HomeInMouse;

    private void Update() {
        if (_TargetPosition == null) {
            //card root lost. Destroying card graphics;
            Destroy(gameObject);
        }
        if (transform.parent != _TargetPosition.transform.parent || HomeInMouse)
            return;

        transform.SetPositionAndRotation(
            Vector3.Lerp(transform.position, _TargetPosition.transform.position, Time.deltaTime * 2),
            Quaternion.Lerp(transform.rotation, _TargetPosition.transform.rotation, Time.deltaTime * 2));
    } 
}
