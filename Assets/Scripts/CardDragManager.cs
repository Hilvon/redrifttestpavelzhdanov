using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine.UI;
public class CardDragManager : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    private CardDataHolder _DataHolder;
    [SerializeField]
    private CardSmoothMovementHandler _CardMover;
    [SerializeField]
    private CardGlowHandler _GlowHandler;
    [SerializeField]
    private Image _RayBlocker;
    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
        _CardMover.HomeInMouse = true;
        _RayBlocker.raycastTarget = false;
        _GlowHandler.AddRequest(DragGlow);
    }

    public void ConfirmDropzone(CardDropZone zone) {
        _DataHolder.transform.SetParent(zone.transform, false);
        _CardMover.transform.SetParent(zone.transform, true);
    }

    private bool _WasOverDropzone = false;
    void IDragHandler.OnDrag(PointerEventData eventData) {
        var cardspace = _CardMover.transform.parent as RectTransform;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(cardspace, Input.mousePosition, Camera.main, out var locaPos)) {
            _CardMover.transform.localPosition = locaPos+Vector2.right;
        }
        var isDropable = eventData.hovered.Any(_ => _.GetComponent<CardDropZone>() != null);
        if (!_WasOverDropzone && isDropable) {
            _WasOverDropzone = true;
            _GlowHandler.AddRequest(DropableGlow);
        }
        else if (_WasOverDropzone && !isDropable) {
            _WasOverDropzone = false;
            _GlowHandler.RemoveRequest(DropableGlow);
        }
            
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData) {
        _CardMover.HomeInMouse = false;
        _RayBlocker.raycastTarget = true;
        _GlowHandler.RemoveRequest(DragGlow);
        if (_WasOverDropzone) {
            _WasOverDropzone = false;
            _GlowHandler.RemoveRequest(DropableGlow);
        }
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData) {
        _GlowHandler.AddRequest(HowerGlow);
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData) {
        _GlowHandler.RemoveRequest(HowerGlow);
    }

    private static CardGlowHandler.GlowRequestData HowerGlow = new CardGlowHandler.GlowRequestData(Color.cyan, 1);
    private static CardGlowHandler.GlowRequestData DragGlow = new CardGlowHandler.GlowRequestData(Color.yellow, 2);
    private static CardGlowHandler.GlowRequestData DropableGlow = new CardGlowHandler.GlowRequestData(Color.green, 3);

}
