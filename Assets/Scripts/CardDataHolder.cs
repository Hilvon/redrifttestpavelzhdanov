using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class CardDataHolder : MonoBehaviour
{
    [SerializeField]
    private ReactiveVarible<Sprite> _Portrait;

    [SerializeField]
    private ReactiveVarible<string> _Title;

    [SerializeField]
    private ReactiveVarible<string> _Description;

    [SerializeField]
    private IntReactiveValue _Attack;

    [SerializeField]
    private IntReactiveValue _HP;

    [SerializeField]
    private IntReactiveValue _Mana;

    public IntReactiveValue Attack => _Attack;
    public IntReactiveValue HP => _HP;
    public IntReactiveValue Mana => _Mana;

    public ReactiveVarible<Sprite> Portrait => _Portrait;
    public ReactiveVarible<string> Title => _Title;
    public ReactiveVarible<string> Description => _Description;

    public event Action OnCardReset;
    public bool SkipAnimations { get; private set; }

    public void SetupCardData(CardClassData data) {
        SkipAnimations = true;
        _Portrait.Value = data.Portrait;
        _Title.Value = data.Title;
        _Description.Value = data.Description;
        _Attack.Value = data.InitialAttack;
        _HP.Value = data.InitialHP;
        _Mana.Value = data.InitialMana;
        SkipAnimations = false;
        OnCardReset?.Invoke();
    }

    public void UpdateRandomValue() {
        var targetValue = Random.Range(0,3);
        switch (targetValue) {
            case 0:
                UpdateTargetVariable(_Attack);
            break;
            case 1:
                UpdateTargetVariable(_HP);
            break;
            default:
                UpdateTargetVariable(_Mana);
            break;
        }
    }

    private void UpdateTargetVariable(IntReactiveValue target) {
        var initValue = target.Value;
        var newValue = Random.Range(2, 10);
        while (newValue == initValue)
            newValue = Random.Range(2, 10);
        target.Value = newValue;
    }
}
