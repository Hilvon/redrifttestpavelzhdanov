using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ReactiveVarible), true)]
public class ReactiveValueEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        var valueProterty = property.FindPropertyRelative("_Value");
        var labelWidth = position.width / 3f;
        var objValue = property.serializedObject.targetObject.GetType()
            .GetField(property.propertyPath, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic)?
            .GetValue(property.serializedObject.targetObject) as ReactiveVarible;


        GUI.Label(new Rect(position.position, new Vector2(labelWidth, position.height)), label);
        EditorGUI.BeginChangeCheck();
        EditorGUI.PropertyField(new Rect(position.position + Vector2.right * labelWidth, position.size - Vector2.right * labelWidth), valueProterty, GUIContent.none, true);
        property.serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck()) {
            objValue?.ForceUpdate();
        }
    }
}
