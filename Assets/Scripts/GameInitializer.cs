using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInitializer : MonoBehaviour
{
    [SerializeField]
    private int _CardClassCount;
    [SerializeField]
    private bool _OverrideSpawnCardCount;
    [SerializeField]
    private int _SpawnedCardCount;

    [SerializeField]
    private CardClassManager _ClassManager;
    [SerializeField]
    private CardSpawnManager _CardSpawner;
    [SerializeField]
    private HandCardsContainer _Hand;

    // Start is called before the first frame update
    void Start()
    {
        _ClassManager.InitializeCardClasses(_CardClassCount, () => {
            var classLibrary = _ClassManager.GetClassNames;
            var count = _OverrideSpawnCardCount ? _SpawnedCardCount : Random.Range(4, 7);
            for (int i = 0; i < count; i++) {
                var pickedClass = classLibrary[Random.Range(0, classLibrary.Length)];
                var newCard = _CardSpawner.CreateCard(pickedClass);
                _Hand.AddCard(newCard);
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
