using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

public class CardClassManager : MonoBehaviour
{
    int classGenerationRunning = 0;
    public void InitializeCardClasses(int count, Action callback) {
        _GeneratedClasses = new Dictionary<string, CardClassData>();
        var templates = CardTemplateManager.GetAvailableTemplates().ToArray();
        Debug.Log($"{templates.Length} templates are available");
        for (int i = 0; i < count; i++) {
            classGenerationRunning++;
            StartCoroutine(GetTexture((portrait) => {
                Debug.Log("Got image. " + _GeneratedClasses.Count);
                _GeneratedClasses.Add("Class" + _GeneratedClasses.Count, new CardClassData(
                    portrait,
                    "Title" + _GeneratedClasses.Count,
                    "Description text for card Title" + _GeneratedClasses.Count,
                    Random.Range(2, 10),
                    Random.Range(2, 10),
                    Random.Range(2, 10),
                    templates[Random.Range(0, templates.Length)]
                    ));
                classGenerationRunning--;
            }));
        }
        StartCoroutine(WaitForAllComplete(callback));

        //StartCoroutine(RunInitializationsOneByOne(count, callback));
    }

    public CardClassData GetClass(string className) { 
        if (_GeneratedClasses != null && _GeneratedClasses.TryGetValue(className, out var card)) {
            return card;
        }
        return null;
    }
    public string[] GetClassNames => _GeneratedClasses.Keys.ToArray();

    private Dictionary<string, CardClassData> _GeneratedClasses;


    private IEnumerator GetTexture(Action<Sprite> callback) {
        var attempCount = 0;
        while (attempCount < 10) {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture("https://picsum.photos/200/200");
            yield return www.SendWebRequest();
            if (www.result != UnityWebRequest.Result.Success) {
                Debug.Log(www.error);
                attempCount++;
            }
            else {
                Texture2D myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                callback(Sprite.Create(myTexture, new Rect(0, 0, myTexture.width, myTexture.height), new Vector2(0, 0)));
                attempCount = 1000;
            }
        } 
    }

    private IEnumerator RunInitializationsOneByOne(int count, Action callback) {
        _GeneratedClasses = new Dictionary<string, CardClassData>();
        var templates = CardTemplateManager.GetAvailableTemplates().ToArray();

        for (int i = 0; i < count; i++) {
            var attempCount = 0;
            while (attempCount < 10) {
                UnityWebRequest www = UnityWebRequestTexture.GetTexture("https://picsum.photos/200/200");
                yield return www.SendWebRequest();
                if (www.result != UnityWebRequest.Result.Success) {
                    Debug.Log(www.error);
                    attempCount++;
                }
                else {
                    Texture2D myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                    var portrait = Sprite.Create(myTexture, new Rect(0, 0, myTexture.width, myTexture.height), new Vector2(0, 0));
                    attempCount = 1000;
                    Debug.Log("Got image for card " + i);
                    _GeneratedClasses.Add("Class" + i, new CardClassData(
                        portrait,
                        "Title" + i,
                        "Description text for card Title" + i,
                        Random.Range(2, 10),
                        Random.Range(2, 10),
                        Random.Range(2, 10),
                        templates[Random.Range(0, templates.Length)]
                    ));
                }
            }

        }
        callback?.Invoke();
    }

    private IEnumerator WaitForAllComplete(Action callback) {
        yield return new WaitWhile(() => classGenerationRunning > 0);
        callback?.Invoke();
    }

}
