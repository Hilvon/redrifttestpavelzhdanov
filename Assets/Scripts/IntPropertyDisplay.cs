using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntPropertyDisplay : MonoBehaviour
{
    public enum DisplayedProperty { Attack, HP, Mana }

    [SerializeField]
    private Text _Display; //May be rplaced with TextMeshPro version if needed;

    [SerializeField]
    private CardDataHolder _DataHolder;

    [SerializeField]
    private AnimationCurve _ChangeAnimationScaleCurve;
    [SerializeField]
    private float _AnimationDuration;
    [SerializeField]
    private float _AnimationValueGhangeMoment;
    [SerializeField]
    private DisplayedProperty _TargetProperty;

    private Vector3 _OriginalScale;
    private Coroutine _RunningAnimation;
    protected IntReactiveValue PickValue(CardDataHolder dataHolder) {
        switch (_TargetProperty) {
            case DisplayedProperty.Attack: return dataHolder.Attack;
            case DisplayedProperty.HP: return dataHolder.HP;
            case DisplayedProperty.Mana: return dataHolder.Mana;
            default: return null;
        }
    }

    private void Awake() {
        _OriginalScale = _Display.transform.localScale;
        var targetProperty = PickValue(_DataHolder);
        _Display.text = targetProperty.Value.ToString();
        targetProperty.OnValueUpdated += OnValueUpdated;
    }

    private void OnValueUpdated(int newValue) {
        if (_DataHolder.SkipAnimations) {
            _Display.text = newValue.ToString();
        }
        else {
            if (_RunningAnimation != null)
                StopCoroutine(_RunningAnimation);
            _RunningAnimation = StartCoroutine(RunAnimation(newValue));
        }
    }

    private IEnumerator RunAnimation(int newValue) {
        var curTime = 0f;
        var isValueChanged = false;
        while (curTime < 1) {
            if (!isValueChanged && curTime >= _AnimationValueGhangeMoment) {
                isValueChanged = true;
                _Display.text = newValue.ToString();
            }
            _Display.transform.localScale = _OriginalScale * _ChangeAnimationScaleCurve.Evaluate(curTime);
            yield return null;
            curTime += Time.deltaTime / _AnimationDuration;
        }
        _Display.text = newValue.ToString();
        _Display.transform.localScale = _OriginalScale;
        _RunningAnimation = null;
    }
}
