using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardClassData
{
    public Sprite Portrait { get; private set; }
    public string Title { get; private set; }
    public string Description { get; private set; }
    public int InitialAttack { get; private set; }
    public int InitialHP { get; private set; }
    public int InitialMana { get; private set; }

    public string TemplateType { get; private set; }

    public CardClassData(Sprite portrait, string title, string description, int initialAttack, int initialHP, int initialMana, string templateType) {
        this.Portrait = portrait;
        this.Title = title;
        this.Description = description;
        this.InitialAttack = initialAttack;
        this.InitialHP = initialHP;
        this.InitialMana = initialMana;
        this.TemplateType = templateType;
    }
}
