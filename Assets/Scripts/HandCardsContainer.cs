using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HandCardsContainer : MonoBehaviour
{
    [SerializeField]
    private Transform _LeftBorder;
    [SerializeField]
    private Transform _RightBorder;
    [SerializeField]
    private float _CurveSharpness;


    private List<CardDataHolder> _HeldCards = new List<CardDataHolder>();
    public void AddCard(CardDataHolder card) {
        _HeldCards.Add(card);
    }

    private int curTargetCard = -1;
    public void DebugModifyCard() {
        if (_HeldCards.Count == 0)
            return;
        curTargetCard = (curTargetCard+1) % _HeldCards.Count;
        Debug.Log(curTargetCard);
        _HeldCards[curTargetCard].UpdateRandomValue();
    }

    private void Update() {
        _HeldCards.RemoveAll(_ => _ == null || _.transform.parent != transform);
        var step = 1f / _HeldCards.Count;
        var pos = step / 2;
        foreach (var card in _HeldCards) {
            PositionOnCurve(card.transform, pos);
            pos += step;
        }
    }

    private void PositionOnCurve(Transform item, float t) {
        var pos = Vector3.Lerp(
            Vector3.Lerp(_LeftBorder.position, _LeftBorder.position+_LeftBorder.right* _CurveSharpness, t), 
            Vector3.Lerp(_RightBorder.position - _RightBorder.right*_CurveSharpness, _RightBorder.position, t), t);
        var rot = Quaternion.Lerp(_LeftBorder.rotation, _RightBorder.rotation, t);
        item.SetPositionAndRotation(pos, rot);
    }
}
