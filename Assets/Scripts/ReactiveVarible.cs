using System;
using UnityEngine;

[Serializable]
public class ReactiveVarible<T>: ReactiveVarible
{
    [SerializeField]
    private T _Value;
    public T Value {
        get => _Value; set {
            if (!_Value.Equals(value)) {
                _Value = value;
                OnValueUpdated?.Invoke(_Value);
            }
        }
    }
    public event Action<T> OnValueUpdated;

    public override void ForceUpdate() { OnValueUpdated?.Invoke(_Value); }

    public static implicit operator T(ReactiveVarible<T> d) => d.Value;
}

public abstract class ReactiveVarible {
    public abstract void ForceUpdate();
}