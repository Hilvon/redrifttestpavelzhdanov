using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class CardGlowHandler : MonoBehaviour
{
    [SerializeField]
    private Image _Glow;
    public void AddRequest(GlowRequestData request) {
        _AppliedRequests.Add(request);
        RefreshGlow();
    }
    public void RemoveRequest(GlowRequestData request) {
        _AppliedRequests.Remove(request);
        RefreshGlow();
    }

    private List<GlowRequestData> _AppliedRequests = new List<GlowRequestData>();

    private void RefreshGlow() {
        if (_AppliedRequests.Count > 0) {
            _Glow.gameObject.SetActive(true);
            _Glow.color = _AppliedRequests.OrderByDescending(_ => _.priority).First().Color;
        }
        else {
            _Glow.gameObject.SetActive(false);
        }
    }
    public class GlowRequestData
    {
        public Color Color { get; private set; }
        public int priority { get; private set; }

        public GlowRequestData(Color color, int priority) {
            this.Color = color;
            this.priority = priority;
        }
    }
}
