using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class CardTemplateManager 
{
    private static Dictionary<string, CardDataHolder> __AvailableTemplates;
    private static Dictionary<string, CardDataHolder> _AvailableTemplates {
        get {
            if (__AvailableTemplates == null) {
                __AvailableTemplates = Resources.LoadAll<CardDataHolder>("Prefabs/Cards").ToDictionary(_ => _.name, _ => _);
            }
            return __AvailableTemplates;
        }
    }
    public static IEnumerable<string> GetAvailableTemplates() {
        return _AvailableTemplates.Keys;
    }

    public static CardDataHolder GetTemplate(string templateName) {
        if (_AvailableTemplates.TryGetValue(templateName, out var template)) {
            return template;
        }
        return null;
    }
}
